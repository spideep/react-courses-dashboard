import React, { useEffect, useState } from 'react';
import { Row, Navbar, Dropdown, DropdownButton } from 'react-bootstrap';
import Course from './../Course/Course';
import DateChanger from './../DateChanger/DateChanger';
import './CoursesList.scss';

const CoursesList = (props) => {
  const
    base_url = 'http://localhost:3030/',
    courses_url = base_url + 'courses',
    types_url = base_url + 'coursetypes',
    [courses, setCourses] = useState([]),
    [types, setTypes] = useState([]),
    [courseTypeFilter, setCourseTypeFilter] = useState(0),
    [datefilter, setDateFilter] = useState(new Date().toLocaleDateString()),
    [filterCampaign, setFilterCampaign] = useState('All Campaigns');

  let headers = new Headers({
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json"
  });

  let ct_request = new Request(types_url, {
    headers,
    mode: 'cors',
    cache: 'default'
  });

  let c_request = (courseid, releaseDate) => {
    courseid = courseid ? "/" + courseid : "/" + courseTypeFilter;
    releaseDate = releaseDate ? "/" + releaseDate : "/" + datefilter;
    return new Request(courses_url + courseid + releaseDate, {
      headers,
      mode: 'cors',
      cache: 'default'
    })
  };

  async function fetchCt() {
    const response = await fetch(ct_request).then((r) => { return r.json() });
    setTypes(response);
  }

  async function fetchCo(courseid, releaseDate) {
    releaseDate = releaseDate ? releaseDate : encodeURIComponent(datefilter);
    courseid = courseid ? courseid : courseTypeFilter;
    let rq = c_request(courseid, releaseDate);
    const response = await fetch(rq).then((r) => { return r.json() });
    setCourses(response);
  }

  useEffect(() => {
    fetchCt();
    fetchCo();
  }, []);

  let HandleDateChange = (date) => {
    setDateFilter(date);
    fetchCo(courseTypeFilter, encodeURIComponent(date));
  }

  let HandleTypeChange = (i, a) => {
    let courseid = parseInt(a.currentTarget.getAttribute('data-value'));
    setCourseTypeFilter(courseid);
    fetchCo(courseid, encodeURIComponent(datefilter));
    setFilterCampaign(courseid ? a.currentTarget.title : 'All Campaigns');
  }

  return (
    <div className="CoursesList">
      <Navbar bg="gray" className="d-flex justify-content-between container-fluid CoursesList__nav">
        <DropdownButton variant="light" size="sm" id="dropdown-basic-button" title={filterCampaign}>
          <Dropdown.Item
            onSelect={HandleTypeChange}
            data-value="0"
            id="item_0"
            key="0"
            {...(courseTypeFilter === 0 ? { className: 'active' } : { className: '' })}
          >All Campaigns</Dropdown.Item>
          {types.map((type, i) => (
            <Dropdown.Item
              onSelect={HandleTypeChange}
              title={type.name}
              data-value={type.id}
              key={type.id}
              id={`item_${type.id}`}
              {...(courseTypeFilter === type.id ? { className: 'active' } : { className: '' })}
            >{type.name}</Dropdown.Item>))}
        </DropdownButton>

        <DateChanger datefilter={datefilter} handleChange={HandleDateChange}></DateChanger>

      </Navbar>

      <div className="d-flex flex-wrap container-fluid pt-3">
        {courses.map(e => {
          return (
            <Course
              key={e.id}
              id={e.id}
              title={e.title}
              subscriptionCost={e.subscriptionCost}
              totalSubscriptions={e.totalSubscriptions}
              totalVacancies={e.totalVacancies}
              status={e.status}
              totalEarned={e.totalEarned}
              totalViews={e.totalViews}
            ></Course>
          )
        })}
      </div>
    </div>
  )
};

CoursesList.propTypes = {};

CoursesList.defaultProps = {};

export default CoursesList;
