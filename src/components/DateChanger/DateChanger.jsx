import React, { useState } from 'react';
import './DateChanger.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

const DateChanger = (props) => {
  const [date, setDate] = useState(props.datefilter);

  let handleClick = (e) => {
    e.preventDefault();
    let ndate;

    switch (e.currentTarget.id) {
      case 'date-left':
        ndate = new Date(date).setDate(new Date(date).getDate() - 1);
        break;
      case 'date-right':
        ndate = new Date(date).setDate(new Date(date).getDate() + 1);
        break;
      default:
        break;
    }

    let daten = new Date(ndate).toLocaleDateString();
    setDate(daten);
    props.handleChange(daten);
  }

  return (
    <div className="DateChanger float-right">
      <button onClick={handleClick} id='date-left'>
        <FontAwesomeIcon icon={faChevronLeft} />
      </button>

      <span className="DateChanger__label mr-2 ml-2">
        {props.datefilter === new Date().toLocaleDateString() ? 'TODAY' : props.datefilter}
      </span>

      <button onClick={handleClick} id='date-right'>
        <FontAwesomeIcon icon={faChevronRight} />
      </button>
    </div>
  )
};

DateChanger.propTypes = {};

DateChanger.defaultProps = {};

export default DateChanger;
