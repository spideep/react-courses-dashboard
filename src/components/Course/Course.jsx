import React from 'react';
import './Course.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faUsers, faMoneyBill } from '@fortawesome/free-solid-svg-icons';

const Course = (props) => {
  const percentageBar = props.totalSubscriptions * 100 / props.totalVacancies;

  const barStyle = {
    backgroundColor: 'gray',
    height: '100%',
    width: percentageBar + '%'
  }

  return (
    <div
      key={props.id}
      id={props.id}
      className="pr-3 pt-3 pl-3 pb-0 mr-2 Course col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2"
    >
      <h6>{props.title}</h6>

      <div className="d-flex justify-content-between mb-2">
        <span className="col pl-0 pr-0 text-left">${props.subscriptionCost} / month</span>
        <span className={`col status_label status_${props.status} pl-0 pr-0 text-right`}>{props.status}</span>
      </div>

      <div className="Course__subsbar">
        <div className='Course__subsbarin' style={barStyle}></div>
      </div>

      {props.status == 'Rejected' ? (
        <div className="Course__bottomrow mt-3 pt-2 pb-2">
          <div className="text-danger ml-auto mr-auto">Card is on hold</div>
        </div>
      ) : (
          <div className="Course__bottomrow mt-3 pt-2 pb-2 d-flex">
            <span className="col pl-1 pr-1">
              <FontAwesomeIcon icon={faMoneyBill} className="pr-1" />
              ${props.totalEarned}
            </span>
            <span className="col pl-1 pr-1">
              <FontAwesomeIcon icon={faUsers} className="pr-1" />
              {props.totalSubscriptions}
            </span>
            <span className="col pl-1 pr-1">
              <FontAwesomeIcon icon={faEye} className="pr-1" />
              {props.totalViews}
            </span>
          </div>
        )}
    </div>
  )
};

Course.propTypes = {
};

Course.defaultProps = {};

export default Course;
