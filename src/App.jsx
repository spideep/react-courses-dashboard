import React from 'react';
import './App.scss';
import CoursesList from './components/CoursesList/CoursesList'
import './../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Container from './../node_modules/react-bootstrap/Container'

function App() {
  return (
    <Container fluid className="App p-0">
      <CoursesList></CoursesList>
    </Container>
  );
}

export default App;
